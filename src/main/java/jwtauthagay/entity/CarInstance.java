package jwtauthagay.entity;

import javax.persistence.*;

@Entity
@Table(name = "car_instance")
public class CarInstance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int year;

    @Column(name = "img_default")
    private String imgDefault;

    @ManyToOne(fetch = FetchType.EAGER)
    private CarMark carMark;
}
