package jwtauthagay.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "t_carAppearance")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarAppearance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pic_url")
    private String picUrl;

    private String color;
}
