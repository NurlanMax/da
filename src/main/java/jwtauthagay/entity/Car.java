package jwtauthagay.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "t_car")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String VIN;

    @Column(name = "engine_volume")
    private float engineVolume;

    @ManyToOne(fetch = FetchType.EAGER)
    private CarAppearance carAppearance;

    @ManyToOne(fetch = FetchType.EAGER)
    private CarInstance carInstance;
}
