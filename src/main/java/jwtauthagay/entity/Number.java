package jwtauthagay.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_number")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Number {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String number;
    @ManyToMany(fetch =FetchType.EAGER)
    private List<Car> cars;
}
