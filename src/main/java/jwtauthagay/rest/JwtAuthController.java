package jwtauthagay.rest;

import jwtauthagay.entity.User;
import jwtauthagay.jwt.JwtTokenGenerator;
import jwtauthagay.model.*;
import jwtauthagay.repo.UserRepo;
import jwtauthagay.service.UserService;
import jwtauthagay.twilio.Service;
import jwtauthagay.twilio.SmsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
@RequestMapping("/api")
public class JwtAuthController {
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Autowired
    private UserRepo userRepo;


    @Autowired
    private Service service;
    @Autowired
    private JwtTokenGenerator jwtTokenGenerator;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;
    @RequestMapping(value ="/auth")
    public ResponseEntity<?> auth(@RequestBody JwtRequest request) throws Exception{
        authenticate(request.getPhoneNumber(),request.getPassword());
        final UserDetails userDetails=userService.loadUserByUsername(request.getPhoneNumber());
        final User user=userService.getUserByPhoneNumber(request.getPhoneNumber());
        final String token=jwtTokenGenerator.generateToken(userDetails);
        System.out.println("Whats upp");
        return new ResponseEntity<>(new JwtResponse(user.getId(), user.getPhoneNumber(),user.getFullName(),user.getPassword(), user.getRoles(), token), HttpStatus.OK);
    }


    public void authenticate(String phoneNumber, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(phoneNumber,password));
        } catch (DisabledException e) {
            e.printStackTrace();
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @PostMapping(value = "/register")
    public ResponseEntity<?> toRegister(@RequestBody User user){
        User newUser = new User();
        newUser.setFullName(user.getFullName());
        newUser.setPassword(user.getPassword());
        newUser.setPhoneNumber(user.getPhoneNumber());
        newUser.setActivated(user.isActivated());
        if (userService.createUser(newUser)!= null){
            return ResponseEntity.ok(user);
        }
        return ResponseEntity.ok(user);
    }


//    @PostMapping( value = "/sms")
//    public void verifyPhoneNumber(@RequestBody SmsRequest smsRequest) {
//        System.out.println(smsRequest);
//        service.sendSms(smsRequest);
//    }

    Random random=new Random();
    String  rm= "";





    @GetMapping(value = "/getRandNum")
    public ResponseEntity<?> getRandNum(){
        User user=getUser();
        String phoneNumber=user.getPhoneNumber();
        String randomm =String.format("%04d", random.nextInt(10000));
        rm=randomm;
        service.sendSms(phoneNumber, rm);
        System.out.println(rm);
        System.out.println(user);
        assert user!=null;
        return new ResponseEntity<>(new UserDTO(user.getId(), user.getPhoneNumber(), user.getFullName(),user.getPassword(), user.getRoles()), HttpStatus.OK);
    }

    @PostMapping(value = "/verifyAccount")
    public ResponseEntity<?> verifyAccount(@RequestBody RandNumDTO randNumDTO){
        User user=getUser();
        if(randNumDTO.getRandNum().equals(rm)){
            userService.activatePhoneNum(user);
            System.out.println("Number verified");
            return ResponseEntity.ok(randNumDTO);
        }
        return null;
    }

    String numForRecoverPass="";

    @GetMapping(value = "/forgotPassword")
    public ResponseEntity<?> forgotPassword(@RequestBody ForgotPasswordDTO forgotPasswordDTO){
        String phoneNumber=forgotPasswordDTO.getPhoneNumber();
        User user=userService.getUserByPhoneNumber(phoneNumber);
        if(user!=null) {
            String randomm=String.format("%04d", random.nextInt(10000));
            numForRecoverPass=randomm;
            service.sendSms(phoneNumber, numForRecoverPass);
            System.out.println("user with given phone number exists");
        }

        return null;
    }

    @PostMapping(value = "/recoverPassword")
    public ResponseEntity<?> recoverPassword(@RequestBody RecoverPasswordDTO recoverPasswordDTO){
        System.out.println(numForRecoverPass);
        System.out.println(recoverPasswordDTO.getForDigitsNum());
        System.out.println(recoverPasswordDTO.getNewPassword().equals(recoverPasswordDTO.getReNewPassword()));
        System.out.println(recoverPasswordDTO.getPhoneNum());
        if(numForRecoverPass.equals(recoverPasswordDTO.getForDigitsNum()) &&
                recoverPasswordDTO.getNewPassword().equals(recoverPasswordDTO.getReNewPassword())){
            User user=userService.getUserByPhoneNumber(recoverPasswordDTO.getPhoneNum());
            user.toString();
            user.setPassword(passwordEncoder.encode(recoverPasswordDTO.getNewPassword()));
            userRepo.save(user);
            System.out.println("Pass recovered");
        }
        System.out.println("check condition statement");
        return null;
    }
//    @GetMapping(value = "/getRandNum")
//    public ResponseEntity<?> getRandNum(){
//        User user=getUser();
//        String phoneNumber=user.getPhoneNumber();
//        service.sendSms(phoneNumber, rm);
//        System.out.println(rm);
//        System.out.println(user);
//        assert user!=null;
//        return new ResponseEntity<>(new UserDTO(user.getId(), user.getPhoneNumber(), user.getFullName(),user.getPassword(), user.getRoles()), HttpStatus.OK);
//    }
    @GetMapping
    private User getUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!(authentication instanceof AnonymousAuthenticationToken)){
            User user = (User) authentication.getPrincipal();
            return user;
        }
        return null;
    }
}
