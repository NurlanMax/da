package jwtauthagay.rest;

import jwtauthagay.entity.User;
import jwtauthagay.model.ForgotPasswordDTO;
import jwtauthagay.model.RecoverPasswordDTO;
import jwtauthagay.service.UserService;
import jwtauthagay.twilio.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
@RequestMapping("/main")
public class MainController {

    @Autowired
    private UserService userService;

    @Autowired
    private Service service;

    @GetMapping("/welcome")
    public String welcome() {
        System.out.println("welcome");
        return "Welcome";
    }

//    Random random = new Random();
//
//    String numForRecoverPass = String.format("%04d", random.nextInt(10000));
//
//    @GetMapping(value = "/forgotPassword")
//    public ResponseEntity<?> forgotPassword(@RequestBody ForgotPasswordDTO forgotPasswordDTO) {
//        String phoneNumber = forgotPasswordDTO.getPhoneNumber();
//        User user = userService.getUserByPhoneNumber(phoneNumber);
//        if (user != null) {
//            service.sendSms(phoneNumber, numForRecoverPass);
//            System.out.println("user with given phone number exists");
//        }
//        return null;
//    }
//
//    @PostMapping(value = "/recoverPassword")
//    public ResponseEntity<?> recoverPassword(@RequestBody RecoverPasswordDTO recoverPasswordDTO) {
//        if (numForRecoverPass.equals(recoverPasswordDTO.getForDigitsNum()) &&
//                recoverPasswordDTO.getNewPassword().equals(recoverPasswordDTO.getReNewPassword())) {
//            System.out.println("number is correct and password is correct");
//            User user = userService.getUserByPhoneNumber(recoverPasswordDTO.getPhoneNum());
//            userService.recoverPassword(user);
//            System.out.println("Pass recovered");
//        }
//        System.out.println("check condition statement");
//        return null;
//    }
}
