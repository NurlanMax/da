package jwtauthagay.twilio;

public interface SmsSender {

    void sendSms(String phoneNumber, String rdNum);

    // or maybe void sendSms(String phoneNumber, String message);
}