package jwtauthagay.twilio;

import javax.validation.constraints.NotBlank;

public class SmsRequest {

    @NotBlank
    private final String number; // destination

    @NotBlank
    private final String message;

    public SmsRequest(@NotBlank String number, @NotBlank String message) {
        this.number = number;
        this.message = message;
    }


//    public SmsRequest(@JsonProperty("number") String number,
//                      @JsonProperty("message") String message) {
//        this.number = number;
//        this.message = message;
//    }

    public String number() {
        return number;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "SmsRequest{" +
                "number= ..." + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
