package jwtauthagay.twilio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@org.springframework.stereotype.Service
public class Service {

    private SmsSender smsSender;

    @Autowired
    public Service(@Qualifier("twilio")TwilioSmsSender smsSender) {
        this.smsSender = smsSender;
    }

    public void sendSms(String phoneNumber, String randNum) {
        smsSender.sendSms(phoneNumber,randNum);
    }
}
