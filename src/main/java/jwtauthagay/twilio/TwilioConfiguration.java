package jwtauthagay.twilio;

import org.springframework.context.annotation.Configuration;

@Configuration
public class TwilioConfiguration {

    private String accountSid="ACd4695d91347a63ea3788d8a2172a2843";
    private String authToken="4a4c7c311cf4a405763f6ac0e1199c2c";
    private String trialNumber="+19804306763";

    public TwilioConfiguration() {

    }

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getTrialNumber() {
        return trialNumber;
    }

    public void setTrialNumber(String trialNumber) {
        this.trialNumber = trialNumber;
    }
}