package jwtauthagay.service;

import jwtauthagay.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User getUserByPhoneNumber(String phoneNumber);

    User createUser(User user);

    User recoverPassword(User user);
    User activatePhoneNum(User user);
}
