package jwtauthagay.service.impl;

import jwtauthagay.entity.Role;
import jwtauthagay.entity.User;
import jwtauthagay.repo.RoleRepo;
import jwtauthagay.repo.UserRepo;
import jwtauthagay.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepo.findByPhoneNumber(s);
        if (user != null) {
            return user;
        } else {
            throw new UsernameNotFoundException("User Not Found");
        }
    }

    @Override
    public User getUserByPhoneNumber(String phoneNumber) {
        return userRepo.findByPhoneNumber(phoneNumber);
    }

    @Override
    public User createUser(User user) {
        User checkUser = userRepo.findByPhoneNumber(user.getPhoneNumber());
        if (checkUser == null) {
            Role role = roleRepo.findByRole("ROLE_USER");
            if (role != null) {
                ArrayList<Role> roles = new ArrayList<>();
                roles.add(role);
                user.setActivated(false);
                user.setRoles(roles);
                user.setPassword(passwordEncoder.encode(user.getPassword()));
                return userRepo.save(user);
            }
        }
        return null;
    }

    @Override
    public User recoverPassword(User user) {
        if (user!=null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepo.save(user);
        }
        return null;
    }


//    @Override
//    public User changePassword(User user) {
//        if(user != null){
//            user.setPassword(passwordEncoder.encode(user.getPassword()));
//            return userRepo.save(user);
//
//        }
//
//        return null;
//    }

    @Override
    public User activatePhoneNum(User user) {
        if(user!=null) {
            user.setActivated(true);
            return userRepo.save(user);
        }
        return null;
    }
}
