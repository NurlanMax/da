package jwtauthagay.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import jwtauthagay.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenGenerator jwtTokenGenerator;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

        final String requestTokenHeader=request.getHeader("Authorization");
        String phoneNumber=null;
        String jwtToken=null;

        if(requestTokenHeader!=null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken=requestTokenHeader.substring(7);
            try {
                phoneNumber=jwtTokenGenerator.getPhoneNumberFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                System.out.println("Unable get Jwt TOKEN");
            } catch (ExpiredJwtException e) {
                e.printStackTrace();
                System.out.println("JWT expired");

            }
        } else {
            System.out.println("Header doesnt apears with Bearer ");
        }
        if(phoneNumber!=null && SecurityContextHolder.getContext().getAuthentication()==null) {
            UserDetails userDetails=userService.loadUserByUsername(phoneNumber);
            if(jwtTokenGenerator.validateToken(jwtToken,userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=
                        new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }


        chain.doFilter(request, response);
    }
}
