package jwtauthagay.model;

import jwtauthagay.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponse implements Serializable {

    private static final long serialVersionUID = 987654321L;


    private Long id;
    private String phoneNumber;
    private String fullName;
    private String password;
    private List<Role> roles;
    private String jwtToken;

//    public JwtResponse(String jwtToken){
//        this.jwtToken = jwtToken;
//    }

    public String getJwtToken(){
        return this.jwtToken;
    }


}
