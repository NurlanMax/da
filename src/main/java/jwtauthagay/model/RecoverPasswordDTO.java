package jwtauthagay.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecoverPasswordDTO {
    private String phoneNum;
    private String forDigitsNum;
    private String newPassword;
    private String reNewPassword;
}
