package jwtauthagay.model;

import jwtauthagay.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable{
    private Long id;
    private String phoneNumber;
    private String fullName;
    private String password;
    private List<Role> roles;

}
